package com.ezclap.taigacorp.tp1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class Adapter  extends ArrayAdapter{

    private final Activity context;

    private final int[] imageArray;

    private final String[] nameArray;



    public Adapter (Activity context, String[] nameArrayParam, int[] imageIDArrayParam) {

        super(context, R.layout.list_row, nameArrayParam);
        this.context = context;
        this.nameArray = nameArrayParam;
        this.imageArray = imageIDArrayParam;
    }
        public View getView(int position, View view, ViewGroup parent){
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.list_row, null,true);
            TextView nameTextField = (TextView) rowView.findViewById(R.id.nameId);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageID);

            nameTextField.setText(nameArray[position]);
            imageView.setImageResource(imageArray[position]);
            return rowView;
        }


}
