package com.ezclap.taigacorp.tp1;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    CountryList cL = new CountryList();

    String[] nameArray = cL.getNameArray();
    String[] imageArray = cL.getImageArray();
    int[] imageIDArray = new int[imageArray.length];


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for (int  i = 0; i <imageArray.length; i++) {
            imageIDArray[i] = this.getResources().getIdentifier(imageArray[i],"drawable", this.getPackageName());
        }
        ListView list = (ListView) findViewById(R.id.list);
        Adapter listAdapter  = new Adapter(this, nameArray, imageIDArray);
        list.setAdapter(listAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Intent infoIntent = new Intent(MainActivity.this, infoActivity.class);
                infoIntent.putExtra("countryName", parent.getItemAtPosition(position).toString());
                startActivity(infoIntent);

            }
        });


    }


}
